AlquilerCoches

Proyecto Symfony versión 2.7. http://symfony.com/ 
En la carpeta  "app/config" está toda la configuración del proyecto: acceso a la base de datos, cuenta desde la que enviar el correo electrónico de confirmación de registro, etc. Para que funcione el envío de correos, es necesario ejecutar la aplicación en el servidor de desarrollo: desde una terminal, en la carpeta del proyecto, ejecutar "php app/console server:start". Nota: El desarrollo de la aplicación se ha hecho utilizando OSX, en Linux funciona igual pero en Windows quizá cambie. 
La base de datos se ha montado en local, por lo que hay que cambiar los datos de acceso en app/config/parameters.yml
Una vez heco lo anterior, para crear la base de datos MySQL en el servidor especificado se debe ejecutar "php app/console doctrine:database:create" , posteriormente, "php app/console doctrine:schema:update --force" para que se haga el mapping entre los objetos PHP y las tablas SQL (crea estas últimas). 
Para añadir un coche hace falta ser admministrador. Para ello, registrar un usuario (de nombre de usuario, por ejemplo, admin) y, después, ejecutar "php app/console fos:user:promote admin ROLE_ADMIN"