<?php
// src/AppBundle/Form/RegistrationType.php

// ver http://symfony.com/doc/current/bundles/FOSUserBundle/overriding_forms.html

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('nombre');
        $builder->add('direccion');
    }

    public function getParent() {
        return 'fos_user_registration';
    }

    public function getName() {
        return 'app_user_registration';
    }

    public function getDireccion() {
        return 'app_user_registration';
    }
}