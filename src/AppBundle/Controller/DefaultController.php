<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller {

	/**
	* @Route("/", name="inicio")
	*/
	public function indexAction() {
		//Obtener los coches de la BD
		//$repository = $this->getDoctrine()
        //->getRepository('AppBundle:Coche');
    	//$coches = $repository->findAll();
		
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
    		'SELECT c
    		FROM AppBundle:Coche c
    		WHERE c.disponible < CURRENT_DATE()'
		);
		$coches = $query->getResult();



		//Renderizar página inicial. Se le pasan los coches de la BD
		return $this->render('inicio/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'coches' => $coches,
        ));
	}

}