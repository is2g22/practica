<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Reserva;
use AppBundle\Entity\Coche;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class ReservaController extends Controller {


	/*  Renderizar página para reservar coche elegido en página principal */
	/**
	* @Route("/reservar/{matricula}", name="reservar")
	*/
	public function indexAction($matricula, Request $request){

		$coche = $this->getDoctrine()
    	->getRepository('AppBundle:Coche')
    	->find($matricula);

    	 if (!$coche) {
        throw $this->createNotFoundException(
            'No product found for id '.$matricula
        );
    }

    	$reserva = new Reserva();
    	$reserva->setCoche($coche);
    	$user=$this->getUser();
    	$reserva->setUser($user);
    	//El id es la concatenación del nombre de usuario, la matrícula del coche y un número aleatorio entre 0 y 100
    	$randomNum=rand(0, 100);
    	$reserva_id=$matricula . $user . $randomNum;
    	$reserva->setId($reserva_id);

    	$form = $this->createFormBuilder($reserva)
			->add('fechaInicio', 'date', array(
				'input' => 'datetime',
				'widget' => 'choice',
				'years' => array('2015', '2016'),
			))
			->add('fechaFin', 'date', array(
				'input' => 'datetime',
				'widget' => 'choice',
				'years' => array('2015', '2016'),
			))
			->add('guardar', 'submit', array('label' => 'Reservar coche'))
			->getForm();

			$form->handleRequest($request);
			//Formulario correcto: añadir reserva a la BD
			if ($form->isValid()) { 
				$em = $this->getDoctrine()->getManager();
    			$em->persist($reserva);
    			//Modificar disponibilidad del coche
    			$coche->setDisponible($reserva->getFechaFin());
    			$em->persist($coche);
    			$em->flush();



    			return $this->redirectToRoute('reserva_success', array('reserva_id' => $reserva->getId()));
			}


			return $this->render('reserva/reserva.html.twig', array(
            'form' => $form->createView(),
            'reserva' => $reserva,
        ));


	}

	/**
	* @Route("/reservar/{reserva_id}/exito", name="reserva_success")
	*/
	public function reservaCreadaAction($reserva_id, Request $request){
		$reserva = $this->getDoctrine()
    	->getRepository('AppBundle:Reserva')
    	->find($reserva_id);
		return $this->render('reserva/reservaCreada.html.twig', array(
			'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
			'reserva' => $reserva,
         ));
	}

}