<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Coche;
use AppBundle\Entity\Gama;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/* Controlador para lo relacionado con la administración: añadir/modificar estado coches, etc. */
class AdminController extends Controller {

	/* Indice administración */
	/**
	* @Route("/admin", name="admin")
	*/
	public function indexAction(){
		return $this->render('admin/index.html.twig', array(
			'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
         ));
	}


	/**
	* @Route("/admin/addcoche", name="addcoche")
	*/
	public function addCocheAction(Request $request){
		$coche = new Coche();

		$form = $this->createFormBuilder($coche)
			->add('id', 'text')
			->add('marca', 'text')
			->add('modelo', 'text')
			->add('gama', 'choice', array(
				'choices' => array(
					'Alta' => Gama::GAMA_ALTA,
					'Media' => Gama::GAMA_MEDIA,
					'Baja' => Gama::GAMA_BAJA,
				),
				'choices_as_values' => true,
			))
			->add('guardar', 'submit', array('label' => 'Añadir coche'))
			->getForm();

			$form->handleRequest($request);
			//Formulario correcto: añadir coche a la BD
			if ($form->isValid()) { 
				$em = $this->getDoctrine()->getManager();
    			$em->persist($coche);
    			$em->flush();

    			return $this->redirectToRoute('coche_success');
			}


			return $this->render('admin/addcoche.html.twig', array(
            'form' => $form->createView(),
        ));
	}

	/**
	* @Route("/admin/addcoche/exito", name="coche_success")
	*/
	public function cocheAnyadidoAction(){
		return $this->render('admin/cocheAnyadido.html.twig', array(
			'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
         ));
	}

}