<?php

// src/AppBundle/Entity/Reserva.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\Coche;

/**
 * @ORM\Entity
 */
class Reserva {

	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     */
	protected $id;
	
	/**
     * @ORM\ManyToOne(targetEntity="Coche", inversedBy="reservas")
     * @ORM\JoinColumn(name="coche_id", referencedColumnName="id")
     */
    protected $coche;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reservas")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    protected $fechaInicio;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    protected $fechaFin;

    /**
     * @ORM\ManyToOne(targetEntity="Tarifa", inversedBy="reservas")
     * @ORM\JoinColumn(name="tarifa_id", referencedColumnName="id")
     */
    protected $tarifa;

    /**
    * @ORM\ManyToOne(targetEntity="TarjetaCredito", inversedBy="reservas")
    * @ORM\JoinColumn(name="tarjetaCredito_numero", referencedColumnName="numero")
    */
    private $tarjetaCredito;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Reserva
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coche
     *
     * @param \AppBundle\Entity\Coche $coche
     *
     * @return Reserva
     */
    public function setCoche(\AppBundle\Entity\Coche $coche = null)
    {
        $this->coche = $coche;

        return $this;
    }

    /**
     * Get coche
     *
     * @return \AppBundle\Entity\Coche
     */
    public function getCoche()
    {
        return $this->coche;
    }


    /**
     * Set tarifa
     *
     * @param \AppBundle\Entity\Tarifa $tarifa
     *
     * @return Reserva
     */
    public function setTarifa(\AppBundle\Entity\Tarifa $tarifa = null)
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * Get tarifa
     *
     * @return \AppBundle\Entity\Tarifa
     */
    public function getTarifa()
    {
        return $this->tarifa;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Reserva
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return Reserva
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     *
     * @return Reserva
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set tarjetaCredito
     *
     * @param \AppBundle\Entity\TarjetaCredito $tarjetaCredito
     *
     * @return Reserva
     */
    public function setTarjetaCredito(\AppBundle\Entity\TarjetaCredito $tarjetaCredito = null)
    {
        $this->tarjetaCredito = $tarjetaCredito;

        return $this;
    }

    /**
     * Get tarjetaCredito
     *
     * @return \AppBundle\Entity\TarjetaCredito
     */
    public function getTarjetaCredito()
    {
        return $this->tarjetaCredito;
    }

    //Calcular el precio de una reserva a partir de la duración del alquiler y la gama del coche
    public function calcularPrecio(){
        $dias = $this->getFechaFin()->diff($this->getFechaInicio())->format("%d");
        //Gama baja: 10 €/día, gama media: 15 €/día, gama alta: 20 €/día
        $gama = $this->getCoche()->getGama();
        if ($gama == 'baja'):
            $eurosDiarios = 10;
        elseif ($gama == 'media'):
             $eurosDiarios = 15;
        else:
             $eurosDiarios = 20;
        endif;
        
        return $dias * $eurosDiarios;
    }
}
