<?php

// src/AppBundle/Entity/Companyia.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="id ya existente")
 */
class Companyia {
	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank
     */
	protected $id;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Companyia
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
