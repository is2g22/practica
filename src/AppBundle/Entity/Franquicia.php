<?php

// src/AppBundle/Entity/Franquicia.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="id ya existente")
 */
class Franquicia {
	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank
     */
	protected $id;

	/** 
	* @ORM\Column(type="string", length=16) 
	*/
	protected $ciudad;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Franquicia
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     *
     * @return Franquicia
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }
}
