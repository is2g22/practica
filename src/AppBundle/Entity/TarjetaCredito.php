<?php

// src/AppBundle/Entity/TarjetaCredito.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="numero", message="Tarjeta ya registrada")
 */
class TarjetaCredito {
	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank
     */
	protected $numero;

	/**
	* @ORM\Column(type="string", length=4)
	* @Assert\NotBlank
	*/
	protected $codSeguridad;

	/**
	* @ORM\Column(type="datetime")
	* @Assert\NotBlank
	*/
	protected $fechaCaducidad;

    /**
     * @ORM\ManyToOne(targetEntity="CompTarjetas", inversedBy="tarjetasCredito")
     * @ORM\JoinColumn(name="compTarjetas_id", referencedColumnName="id")
     */
    protected $compTarjetas;


    /**
     * @ORM\OneToMany(targetEntity="Reserva", mappedBy="tarjetaCredito")
     */
    private $reservas;

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return TarjetaCredito
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set codSeguridad
     *
     * @param string $codSeguridad
     *
     * @return TarjetaCredito
     */
    public function setCodSeguridad($codSeguridad)
    {
        $this->codSeguridad = $codSeguridad;

        return $this;
    }

    /**
     * Get codSeguridad
     *
     * @return string
     */
    public function getCodSeguridad()
    {
        return $this->codSeguridad;
    }

    /**
     * Set fechaCaducidad
     *
     * @param \DateTime $fechaCaducidad
     *
     * @return TarjetaCredito
     */
    public function setFechaCaducidad($fechaCaducidad)
    {
        $this->fechaCaducidad = $fechaCaducidad;

        return $this;
    }

    /**
     * Get fechaCaducidad
     *
     * @return \DateTime
     */
    public function getFechaCaducidad()
    {
        return $this->fechaCaducidad;
    }

    /**
     * Set compTarjetas
     *
     * @param \AppBundle\Entity\CompTarjetas $compTarjetas
     *
     * @return TarjetaCredito
     */
    public function setCompTarjetas(\AppBundle\Entity\CompTarjetas $compTarjetas = null)
    {
        $this->compTarjetas = $compTarjetas;

        return $this;
    }

    /**
     * Get compTarjetas
     *
     * @return \AppBundle\Entity\CompTarjetas
     */
    public function getCompTarjetas()
    {
        return $this->compTarjetas;
    }

}
