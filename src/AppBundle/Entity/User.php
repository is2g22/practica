<?php
// src/AppBundle/Entity/User.php
// ver https://symfony.com/doc/master/bundles/FOSUserBundle/index.html
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("email")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    //El email ya está en la clase BaseUser

    //Ver http://symfony.com/doc/current/bundles/FOSUserBundle/overriding_forms.html
     /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Este campo no puede estar vacío.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *  min=3,
     *  max=255,
     *  minMessage="Nombre demasiado corto.",
     *  maxMessage="Nombre demasiado largo.",
     *  groups={"Registration", "Profile"}
     * )
     */
    protected $nombre;

      /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Este campo no puede estar vacío.", groups={"Registration", "Profile"})
     */
    protected $direccion;

    /**
    * @ORM\OneToMany(targetEntity="Reserva", mappedBy="user")
    */
    private $reservas;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

     /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return User
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Add reserva
     *
     * @param \AppBundle\Entity\Reserva $reserva
     *
     * @return User
     */
    public function addReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas[] = $reserva;

        return $this;
    }

    /**
     * Remove reserva
     *
     * @param \AppBundle\Entity\Reserva $reserva
     */
    public function removeReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas->removeElement($reserva);
    }

    /**
     * Get reservas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservas()
    {
        return $this->reservas;
    }

    /**
     * Set tarjetaCredito
     *
     * @param \AppBundle\Entity\TarjetaCredito $tarjetaCredito
     *
     * @return User
     */
    public function setTarjetaCredito(\AppBundle\Entity\TarjetaCredito $tarjetaCredito = null)
    {
        $this->tarjetaCredito = $tarjetaCredito;

        return $this;
    }

    /**
     * Get tarjetaCredito
     *
     * @return \AppBundle\Entity\TarjetaCredito
     */
    public function getTarjetaCredito()
    {
        return $this->tarjetaCredito;
    }
}
