<?php

// src/AppBundle/Entity/CompTarjetas.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="Tarjeta ya registrada")
 */
class CompTarjetas {
	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank
     */
	protected $id;

    /**
    * @ORM\OneToMany(targetEntity="TarjetaCredito", mappedBy="compTarjetas")
    */
    protected $tarjetasCredito;

     public function __construct()
    {
        $this->tarjetasCredito = new ArrayCollection();
    }


    /**
     * Set id
     *
     * @param string $id
     *
     * @return CompTarjetas
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add tarjetasCredito
     *
     * @param \AppBundle\Entity\TarjetaCredito $tarjetasCredito
     *
     * @return CompTarjetas
     */
    public function addTarjetasCredito(\AppBundle\Entity\TarjetaCredito $tarjetasCredito)
    {
        $this->tarjetasCredito[] = $tarjetasCredito;

        return $this;
    }

    /**
     * Remove tarjetasCredito
     *
     * @param \AppBundle\Entity\TarjetaCredito $tarjetasCredito
     */
    public function removeTarjetasCredito(\AppBundle\Entity\TarjetaCredito $tarjetasCredito)
    {
        $this->tarjetasCredito->removeElement($tarjetasCredito);
    }

    /**
     * Get tarjetasCredito
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTarjetasCredito()
    {
        return $this->tarjetasCredito;
    }
}
