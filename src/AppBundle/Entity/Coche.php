<?php

// src/AppBundle/Entity/Coche.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Coche {

    //Cosntantes para definir las posibles gamas:
    const GAMA_BAJA = 'baja';
    const GAMA_MEDIA = 'media';
    const GAMA_ALTA = 'alta';

	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=7)
     * @Assert\NotBlank
     */
	protected $id; //matrícula

	/**
	* @ORM\Column(type="string", length=25)
	*/
	protected $marca;

	/**
	* @ORM\Column(type="string", length=25)
	*/
	protected $modelo;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $gama;

    /**
    *@ORM\Column(type="date")
    */    
    protected $disponible; //fecha en la que el coche estará disponible para ser reservado

    //Reserva (en un principio venía de una relación n:m)
    /**
    * @ORM\OneToMany(targetEntity="Reserva", mappedBy="coche")
    */
    protected $reservas;



     public function __construct()
    {
        $this->reservas = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Coche
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marca
     *
     * @param string $marca
     *
     * @return Coche
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     *
     * @return Coche
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    
    /**
     * Add reserva
     *
     * @param \AppBundle\Entity\Reserva $reserva
     *
     * @return Coche
     */
    public function addReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas[] = $reserva;

        return $this;
    }

    /**
     * Remove reserva
     *
     * @param \AppBundle\Entity\Reserva $reserva
     */
    public function removeReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas->removeElement($reserva);
    }

    /**
     * Get reservas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservas()
    {
        return $this->reservas;
    }

    /**
     * Set gama
     *
     * @param string $gama
     *
     * @return Coche
     */
    public function setGama($gama)
    {
        $this->gama = $gama;

        if (!in_array($gama, array(self::GAMA_BAJA, self::GAMA_MEDIA, self::GAMA_ALTA))) {
            throw new \InvalidArgumentException("Gama no válida");
        }
        $this->gama = $gama;

        return $this;
    }

    /**
     * Get gama
     *
     * @return \AppBundle\Entity\Gama
     */
    public function getGama()
    {
        return $this->gama;
    }


    /**
     * Set disponible
     *
     * @param \DateTime $disponible
     *
     * @return Coche
     */
    public function setDisponible($disponible)
    {
        $this->disponible = $disponible;

        return $this;
    }

    /**
     * Get disponible
     *
     * @return \DateTime
     */
    public function getDisponible()
    {
        return $this->disponible;
    }
}
