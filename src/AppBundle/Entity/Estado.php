<?php

// src/AppBundle/Entity/Gama.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="Estado ya existente")
 */
class Estado {

	const ESTADO_DISPONIBLE = 'disponible';
	const ESTADO_REVISION = 'revision';
	const ESTADO_BAJA = 'baja';
	/**
     * @ORM\Id
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank
     */
	protected $id;

	public function setId($id)  {
        if (!in_array($id, array(self::ESTADO_DISPONIBLE, self::ESTADO_REVISION, self::ESTADO_BAJA))) {
            throw new \InvalidArgumentException("Estado no válido");
        }
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
