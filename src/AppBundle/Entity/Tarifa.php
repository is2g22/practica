<?php

// src/AppBundle/Entity/Tarifa.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="id already taken")
 */
class Tarifa {

    //Cosntantes para definir las posibles tarifas:
    const TARIFA_TEMPBAJA = 'tempBaja';
    const TARIFA_TEMPALTA = 'tempAlta';

    /**
    * @ORM\Id
    * @ORM\Column(type="string", length=10)
    * @Assert\NotBlank
    */
    protected $id;

    /**
    * @ORM\OneToMany(targetEntity="Reserva", mappedBy="tarifa")
    */
    protected $reservas;

     public function __construct()
    {
        $this->reservas = new ArrayCollection();
    }


    /**
     * Set id
     *
     * @param string $id
     *
     * @return Tarifa
     */
    public function setId($id)
    {
        if (!in_array($id, array(self::TARIFA_TEMPBAJA, self::TARIFA_TEMPALTA,))) {
            throw new \InvalidArgumentException("Tarifa no válida");
        }
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add reserva
     *
     * @param \AppBundle\Entity\Reserva $reserva
     *
     * @return Tarifa
     */
    public function addReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas[] = $reserva;

        return $this;
    }

    /**
     * Remove reserva
     *
     * @param \AppBundle\Entity\Reserva $reserva
     */
    public function removeReserva(\AppBundle\Entity\Reserva $reserva)
    {
        $this->reservas->removeElement($reserva);
    }

    /**
     * Get reservas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservas()
    {
        return $this->reservas;
    }
}
