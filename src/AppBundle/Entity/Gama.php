<?php

// src/AppBundle/Entity/Gama.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Gama {

    //Cosntantes para definir las posibles gamas:
    const GAMA_BAJA = 'baja';
    const GAMA_MEDIA = 'media';
    const GAMA_ALTA = 'alta';

    /**
    * @ORM\Id
    * @ORM\Column(type="string", length=10)
    * @Assert\NotBlank
    */
    protected $id;

    /**
    * @ORM\OneToMany(targetEntity="Coche", mappedBy="gama")
    */
    protected $coches;

     public function __construct()
    {
        $this->coches = new ArrayCollection();
    }


    /**
     * Set id
     *
     * @param string $id
     *
     * @return Gama
     */
    public function setId($id)
    {
        if (!in_array($id, array(self::GAMA_BAJA, self::GAMA_MEDIA, self::GAMA_ALTA))) {
            throw new \InvalidArgumentException("Gama no válida");
        }
        $this->id = $id;

        return $this;
    }


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Add coch
     *
     * @param \AppBundle\Entity\Coche $coch
     *
     * @return Gama
     */
    public function addCoch(\AppBundle\Entity\Coche $coch)
    {
        $this->coches[] = $coch;

        return $this;
    }

    /**
     * Remove coch
     *
     * @param \AppBundle\Entity\Coche $coch
     */
    public function removeCoch(\AppBundle\Entity\Coche $coch)
    {
        $this->coches->removeElement($coch);
    }

    /**
     * Get coches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoches()
    {
        return $this->coches;
    }
}
